import 'package:flutter/material.dart';

// Colors
// const primary = Colors.green;
const primary = Color(0xFF6D74E2);
const accent = Color(0xFF48BFCA);
const colorText = Colors.black;
const colorTextWhite = Colors.white;
const colorTextSub = Color(0xFF727272);
const colorTags = Color(0xFFECECEC);
const colorBg = Color(0xFFe8e8e8);

// Dimensions
const padding = 20.0;
const padding_s = 15.0;
const padding_xs = 10.0;
const padding_xxs = 5.0;
const radius = 5.0;


// Text stylings
const headline1 = TextStyle(
  fontSize: 97,
  fontWeight: FontWeight.w300,
  letterSpacing: -1.5,
  color: colorText,
);
const headline2 = TextStyle(
  fontSize: 61,
  fontWeight: FontWeight.w300,
  color: colorText,
  letterSpacing: -0.5,
);
const headline3 = TextStyle(
  fontSize: 48,
  fontWeight: FontWeight.w400,
  color: colorText,
);
const headline4 = TextStyle(
  fontSize: 34,
  fontWeight: FontWeight.w400,
  letterSpacing: 0.25,
  color: colorText,
);
const headline5 = TextStyle(
  fontSize: 24,
  fontWeight: FontWeight.w400,
  color: colorText,
);
const headline6 = TextStyle(
  fontSize: 20,
  fontWeight: FontWeight.w500,
  color: colorText,
  letterSpacing: 0.15,
);
const subtitle1 = TextStyle(
  fontSize: 16,
  fontWeight: FontWeight.w400,
  color: colorText,
  letterSpacing: 0.15,
);
const subtitle2 = TextStyle(
  fontSize: 14,
  fontWeight: FontWeight.w500,
  color: colorText,
  letterSpacing: 0.1,
);
const bodyText1 = TextStyle(
  fontSize: 16,
  fontWeight: FontWeight.normal,
  color: colorText,
  letterSpacing: 0.5,
);
const bodyText2 = TextStyle(
  fontSize: 14,
  fontWeight: FontWeight.w400,
  color: colorText,
  letterSpacing: 0.25,
);
const button = TextStyle(
  fontSize: 14,
  fontWeight: FontWeight.w700,
  color: colorTextWhite,
  letterSpacing: 1.25,
);
const caption = TextStyle(
  fontSize: 12,
  color: colorText,
  fontWeight: FontWeight.w400,
  letterSpacing: 0.4,
);
const overline = TextStyle(
  fontSize: 10,
  fontWeight: FontWeight.w400,
  color: colorText,
  letterSpacing: 1.5,
);
