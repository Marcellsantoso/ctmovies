import 'package:ctcorp_test/Model/movie.dart';

class Helper {
  static List<Movie> generateDummyData() {
    List<Movie> arrMovies = [];

    arrMovies.add(Movie(
      id: 1,
      name: "Transformers",
      director: "Michael Bay",
      summary:
          "The noble Autobots and Decepticons, two intergalactic races of robots, crash land on Earth. They battle for Allspark, the ultimate power source, whose location is held by Sam.",
      tags: "Sci-Fi,Action",
    ));

    arrMovies.add(Movie(
      id: 2,
      name: "Transformers 2: Revenge of the Fallen",
      director: "Michael Bay",
      summary:
          "Sam leaves the Autobots to lead a normal life. However, the Decepticons target him and drag him back into the Transformers' war.",
      tags: "Sci-Fi,Action",
    ));

    arrMovies.add(Movie(
      id: 3,
      name: "Transformers 3: Dark of the Moon",
      director: "Michael Bay",
      summary:
          "Sam Witwicky and the Autobots must unravel the secrets of a Cybertronian spacecraft hidden on the Moon before the Decepticons can use it for their own evil schemes.",
      tags: "Sci-Fi,Action",
    ));

    arrMovies.add(Movie(
      id: 4,
      name: "The Greatest Showman",
      director: "Michael Gracey",
      summary:
          "P T Barnum becomes a worldwide sensation in the show business. His imagination and innovative ideas take him to the top of his game.",
      tags: "Fantasy",
    ));

    arrMovies.add(Movie(
      id: 5,
      name: "Army of the Dead",
      director: "Zack Snyder",
      summary:
          "After a zombie outbreak in Las Vegas, a group of mercenaries takes the ultimate gamble by venturing into the quarantine zone for the greatest heist ever.",
      tags: "Horror,Action",
    ));


    arrMovies.add(Movie(
      id: 6,
      name: "The SpongeBob Movie: Sponge on the Run",
      director: "Tim Hill",
      summary:
          "When SpongeBob SquarePants' beloved pet snail Gary goes missing, a path of clues leads SpongeBob and his best friend Patrick to the powerful King Poseidon, who has Gary held captive in the Lost City of Atlantic City. On their mission to save Gary, SpongeBob and the Bikini Bottom gang team up for a heroic and hilarious journey, where they discover nothing is stronger than the power of friendship.",
      tags: "Horror,Action",
    ));

    return arrMovies;
  }
}
