import 'package:auto_route/auto_route.dart';
import 'package:ctcorp_test/Pages/details.dart';

import '../Pages/home.dart';

@MaterialAutoRouter(
    replaceInRouteName: 'Page,Route',
    routes: <AutoRoute>[
      AutoRoute(page: Home, initial: true),
      AutoRoute(page: Details),
    ],
)
class $AppRouter {}