// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

import 'package:auto_route/auto_route.dart' as _i1;
import 'package:flutter/material.dart' as _i2;

import '../Components/home_store.dart' as _i5;
import '../Model/movie.dart' as _i6;
import '../Pages/details.dart' as _i4;
import '../Pages/home.dart' as _i3;

class AppRouter extends _i1.RootStackRouter {
  AppRouter([_i2.GlobalKey<_i2.NavigatorState>? navigatorKey])
      : super(navigatorKey);

  @override
  final Map<String, _i1.PageFactory> pagesMap = {
    Home.name: (routeData) => _i1.MaterialPageX<dynamic>(
        routeData: routeData,
        builder: (data) {
          final args = data.argsAs<HomeArgs>(orElse: () => const HomeArgs());
          return _i3.Home(key: args.key);
        }),
    Details.name: (routeData) => _i1.MaterialPageX<dynamic>(
        routeData: routeData,
        builder: (data) {
          final args = data.argsAs<DetailsArgs>();
          return _i4.Details(
              key: args.key, homestore: args.homestore, movie: args.movie);
        })
  };

  @override
  List<_i1.RouteConfig> get routes => [
        _i1.RouteConfig(Home.name, path: '/'),
        _i1.RouteConfig(Details.name, path: '/Details')
      ];
}

class Home extends _i1.PageRouteInfo<HomeArgs> {
  Home({_i2.Key? key}) : super(name, path: '/', args: HomeArgs(key: key));

  static const String name = 'Home';
}

class HomeArgs {
  const HomeArgs({this.key});

  final _i2.Key? key;
}

class Details extends _i1.PageRouteInfo<DetailsArgs> {
  Details({_i2.Key? key, required _i5.HomeStore homestore, _i6.Movie? movie})
      : super(name,
            path: '/Details',
            args: DetailsArgs(key: key, homestore: homestore, movie: movie));

  static const String name = 'Details';
}

class DetailsArgs {
  const DetailsArgs({this.key, required this.homestore, this.movie});

  final _i2.Key? key;

  final _i5.HomeStore homestore;

  final _i6.Movie? movie;
}
