import 'dart:developer';

import 'package:auto_route/auto_route.dart';
import 'package:ctcorp_test/Components/button.dart';
import 'package:ctcorp_test/Components/home_store.dart';
import 'package:ctcorp_test/Components/typing.dart';
import 'package:ctcorp_test/Components/wgt.dart';
import 'package:ctcorp_test/Model/movie.dart';
import 'package:ctcorp_test/Utils/constants.dart';
import 'package:ctcorp_test/Components/input_autocheck.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:multi_select_flutter/multi_select_flutter.dart';

class Details extends StatefulWidget {
  Movie? movie;
  HomeStore homestore;
  Details({Key? key, required this.homestore, this.movie}) : super(key: key);

  @override
  _DetailsState createState() => _DetailsState();
}

class _DetailsState extends State<Details> {
  final _typingTitle = Typing();
  final _typingDirector = Typing();
  final _typingSummary = Typing();
  final _typingTags = Typing();

  late InputAutoCheck inputTitle, inputDirector, inputSummary;

  @override
  void initState() {
    super.initState();

    _typingTitle.setValue(widget.movie?.name ?? "");
    _typingDirector.setValue(widget.movie?.director ?? "");
    _typingSummary.setValue(widget.movie?.summary ?? "");
    _typingTags.setValue(widget.movie?.tags ?? "");
    _selectedTags =
        _typingTags.text.trim().isNotEmpty ? _typingTags.text.split(",") : [];
  }

  @override
  Widget build(BuildContext context) {
    String title = widget.movie?.name ?? "ADD NEW TITLE";
    return Scaffold(
      appBar: Wgt.appbar(title: title, tag: widget.movie?.id),
      body: body(),
    );
  }

  Widget body() {
    return Container(
        child: SingleChildScrollView(
      padding: EdgeInsets.all(padding),
      child: Column(children: [
        InputAutoCheck(
          value: _typingTitle.text,
          hint: "Movie title",
          handler: (text) {
            _typingTitle.setValue(text);
          },
        ),
        SizedBox(height: padding_s),
        InputAutoCheck(
          value: _typingDirector.text,
          hint: "Director",
          handler: (text) {
            _typingDirector.setValue(text);
          },
        ),
        SizedBox(height: padding_s),
        InputAutoCheck(
          value: _typingSummary.text,
          hint: "Summary",
          minLines: 4,
          handler: (text) {
            _typingSummary.setValue(text);
          },
        ),
        SizedBox(height: padding_s),
        tags(),
        SizedBox(height: padding_s),
        SizedBox(
            width: double.infinity,
            child: Observer(builder: (_) {
              bool isValid = _typingTitle.text != "" &&
                  _typingDirector.text != "" &&
                  _typingSummary.text != "" &&
                  _typingTags.text != "";
              return Button(
                text: "SAVE",
                handler: isValid ? () => save() : null,
              );
            })),
      ]),
    ));
  }

  static List<String> _tags = [
    "Action",
    "Comedy",
    "Fantasy",
    "Horror",
    "Sci-Fi",
  ];
  final _items = _tags.map((tag) => MultiSelectItem<String>(tag, tag)).toList();
  List<String> _selectedTags = [];

  Widget tags() {
    return Container(
      child: MultiSelectBottomSheetField(
        initialChildSize: 0.5,
        selectedColor: accent.withAlpha(50),
        listType: MultiSelectListType.CHIP,
        searchable: true,
        buttonText: Text("Tags", style: bodyText1),
        title: Container(
          padding: EdgeInsets.symmetric(horizontal: padding),
          child: Text("Tags", style: bodyText1),
        ),
        initialValue: _selectedTags,
        items: _items,
        onConfirm: (values) {
          if (values.length > 1)
            _typingTags.setValue(values.join(","));
          else
            _typingTags.setValue(values.join(""));
        },
        chipDisplay: MultiSelectChipDisplay(
          chipColor: accent.withOpacity(0.1),
          textStyle: TextStyle(color: accent),
          onTap: (value) {
            setState(() {
              _selectedTags.remove(value);
            });
          },
        ),
      ),
    );
  }

  void save() {
    var newObject = false;
    if (widget.movie == null) {
      widget.movie = Movie();
      newObject = true;
    }

    widget.movie?.name = _typingTitle.text;
    widget.movie?.director = _typingDirector.text;
    widget.movie?.summary = _typingSummary.text;
    widget.movie?.tags = _typingTags.text;

    if (newObject) {
      widget.homestore.addMovie(widget.movie);
    }

    AutoRouter.of(context).pop();
  }
}
