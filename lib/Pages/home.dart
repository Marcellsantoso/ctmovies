import 'package:auto_route/auto_route.dart';
import 'package:ctcorp_test/Components/home_store.dart';
import 'package:ctcorp_test/Components/wgt.dart';
import 'package:ctcorp_test/Model/movie.dart';
import 'package:ctcorp_test/Utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import '../Router/app_router.gr.dart' as router;

class Home extends StatefulWidget {
  Home({Key? key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  HomeStore homestore = HomeStore();

  @override
  void initState() {
    super.initState();
    homestore.loadMovies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        // backgroundColor: colorBg,
        appBar: Wgt.appbar(title: "CTMovies"),
        body: body(),
        floatingActionButton: btnAdd());
  }

  Widget body() {
    return Container(
        child: Observer(
      builder: (context) => ListView.builder(
        padding: EdgeInsets.symmetric(horizontal: padding, vertical: padding),
        itemCount: homestore.movies.length,
        itemBuilder: (_, index) => cell(homestore.movies[index]),
      ),
    ));
  }

  Widget cell(Movie item) {
    return Container(
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              offset: Offset(0, 5),
              spreadRadius: 3.0,
              color: Color(0x466D74E2),
              blurRadius: 15.0,
            ),
          ],
          shape: BoxShape.rectangle,
          borderRadius: BorderRadius.circular(10),
        ),
        margin: EdgeInsets.symmetric(vertical: padding_xs),
        child: InkWell(
            onTap: () => AutoRouter.of(context)
                .push(router.Details(homestore: homestore, movie: item)),
            child: Container(
                padding: EdgeInsets.all(padding_s),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Hero(
                          tag: "${item.id}",
                          child: Text(
                            "${item.name}",
                            style: headline6,
                            maxLines: 2,
                          )),
                      SizedBox(height: padding_xs),
                      Text(
                        "${item.summary}",
                        style: bodyText2,
                        maxLines: 3,
                        overflow: TextOverflow.ellipsis,
                      ),
                      SizedBox(height: padding_xs),
                      tags(item),
                    ]))));
  }

  Widget tags(Movie item) {
    List<String> tagData = item.tags?.split(",") ?? [];
    return Wrap(
        spacing: padding_xs,
        children: tagData
            .map((e) {
              return Container(
                padding: EdgeInsets.symmetric(
                    vertical: padding_xxs, horizontal: padding_xs),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(padding),
                    color: colorTags),
                child:
                    Text("#$e", style: caption.copyWith(color: colorTextSub)),
              );
            })
            .toList()
            .cast<Widget>());
  }

  Widget btnAdd() {
    return FloatingActionButton(
      onPressed: () {
        AutoRouter.of(context).push(router.Details(homestore: homestore));
      },
      child: Icon(Icons.add, color: Colors.white),
    );
  }
}
