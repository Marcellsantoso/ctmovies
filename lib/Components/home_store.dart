import 'package:ctcorp_test/Model/movie.dart';
import 'package:ctcorp_test/Utils/helper.dart';
import 'package:mobx/mobx.dart';

part 'home_store.g.dart';

class HomeStore = _HomeStore with _$HomeStore;

abstract class _HomeStore with Store {
  @observable
  ObservableList<Movie> movies = ObservableList<Movie>();

  @action
  loadMovies() {
    this.movies.addAll(Helper.generateDummyData());
  }

  @action
  addMovie(movie) {
    this.movies.add(movie);
  }

}