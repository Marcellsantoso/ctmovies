import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class Wgt {
  static AppBar appbar({required title, tag= ""}) {
    return AppBar(
      // backgroundColor: Colors.white,
      // iconTheme: IconThemeData(color: Colors.black),
      // toolbarTextStyle: headline6.copyWith(fontWeight: FontWeight.w700),
      brightness: Brightness.dark, // status bar brightness
      title: Hero(
        tag: "$tag",
        child: Text(
          "$title",
          // style: headline6.copyWith(fontWeight: FontWeight.w700),
        ),
      ),
    );
  }
}
