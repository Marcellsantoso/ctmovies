import 'package:mobx/mobx.dart';
part 'typing.g.dart';


class Typing = TypingBase with _$Typing;

abstract class TypingBase with Store {
  @observable
  String text = "";

  @action
  void setValue(text) {
    this.text = text;
  }
}