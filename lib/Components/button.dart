import 'package:ctcorp_test/Utils/constants.dart';
import 'package:flutter/material.dart';

class Button extends StatelessWidget {
  String text = "";
  Color backgroundColor, textColor;
  var handler;
  Button({
    this.text = "",
    this.handler,
    this.backgroundColor = accent,
    this.textColor = colorTextWhite,
  });
  @override
  Widget build(BuildContext context) {
    return Container(
      child: ElevatedButton(
          style: ButtonStyle(
            shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(radius),
            )),
            padding: MaterialStateProperty.all(
                EdgeInsets.symmetric(vertical: padding_xs)),
            backgroundColor: MaterialStateProperty.resolveWith((states) {
              if (states.contains(MaterialState.disabled)) {
                return backgroundColor.withOpacity(0.5);
              }
              return backgroundColor;
            }),
          ),
          onPressed: handler,
          child: Text('$text',
              style: TextStyle(
                color: textColor,
                fontWeight: FontWeight.w600,
              ))),
    );
  }
}
