import 'package:ctcorp_test/Utils/constants.dart';
import 'package:flutter/material.dart';

class InputAutoCheck extends StatefulWidget {
  String? hint;
  String? value;
  int? minLines;
  var validator;
  var handler;

  InputAutoCheck({
    Key? key,
    this.hint,
    this.value,
    this.validator,
    this.handler,
    this.minLines,
  }) : super(key: key);

  @override
  InputAutoCheckState createState() => InputAutoCheckState();
}

class InputAutoCheckState extends State<InputAutoCheck> {
  final _formKey = GlobalKey<FormState>();
  final controller = TextEditingController();
  final FocusNode _focusNode = new FocusNode();

  @override
  void initState() {
    super.initState();
    controller.text = widget.value ?? "";
    controller.addListener(() {
      if (_formKey.currentState == null) return;
      bool isValid = _formKey.currentState!.validate();

      if (widget.handler != null) {
        if (isValid)
          widget.handler(controller.text);
        else
          widget.handler("");
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: TextFormField(
        onTap: _requestFocus,
        minLines: widget.minLines ?? 1,
        maxLines: widget.minLines ?? 1,
        cursorColor: accent,
        focusNode: _focusNode,
        decoration: InputDecoration(
            hintText: "${widget.hint}",
            labelText: "${widget.hint}",
            labelStyle:
                TextStyle(color: _focusNode.hasFocus ? accent : Colors.black),
            errorBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.red),
              borderRadius: BorderRadius.circular(5),
            ),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: accent),
              borderRadius: BorderRadius.circular(5),
            ),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.grey[300]!),
              borderRadius: BorderRadius.circular(5),
            ),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(5),
            )),
        controller: controller,
        validator: (value) {
          if (widget.validator != null) {
            return widget.validator(value);
          }

          // Default validator
          if (value == null) return "Empty string";
          if (value.length < 3) return "Too short";

          return null;
        },
      ),
    );
  }

  void _requestFocus() {
    setState(() {
      FocusScope.of(context).requestFocus(_focusNode);
    });
  }
}
