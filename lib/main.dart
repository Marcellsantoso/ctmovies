import 'package:ctcorp_test/Router/app_router.gr.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';

import 'Utils/constants.dart';

void main() {
  runApp(MyApp());
}

/*
 * Mobx docs
 * https://pub.dev/packages/mobx
 * https://mobx.netlify.app/guides/when-does-mobx-react
 * 
 * AutoRoute
 * https://pub.dev/packages/auto_route
 * https://autoroute.vercel.app/basics/working_with_paths
 * 
 * flutter packages pub run build_runner watch
 * 
 * Update icon 
 * flutter pub run flutter_launcher_icons:main
 * 
 * Update splash
 * flutter pub run flutter_native_splash:create --path=/Users/marcelsantoso/Data/Flutter/ctcorp_test/flutter_native_splash.yaml 
 */

class MyApp extends StatelessWidget {
  final _appRouter = AppRouter();

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        fontFamily: GoogleFonts.poppins().fontFamily,
        primaryColor: primary,
        accentColor: accent,
        textTheme: TextTheme(
          headline1: headline1,
          headline2: headline2,
          headline3: headline3,
          headline4: headline4,
          headline5: headline5,
          headline6: headline6,
          subtitle1: subtitle1,
          subtitle2: subtitle2,
          bodyText1: bodyText1,
          bodyText2: bodyText2,
          button: button,
          caption: caption,
          overline: overline,
        ),
      ),
      routerDelegate: _appRouter.delegate(),
      routeInformationParser: _appRouter.defaultRouteParser(),
    );
  }
}
