import 'package:mobx/mobx.dart';

class Movie {
  // Action, Comedy, Fantasy, Horror, Sci-Fi
  Movie({this.id, this.name, this.director, this.summary, this.tags}) {
    if (id == null) id = DateTime.now().millisecondsSinceEpoch;
  }
  int? id;

  @observable
  String? name;

  @observable
  String? director;

  @observable
  String? summary;

  @observable
  String? tags;
}
